from django import forms
from homes.models import Appliance, Home, Roof

class ApplianceForm(forms.ModelForm):
    class Meta:
       model = Appliance
       fields = ["nickname","type", "brand", "model","age_of_unit", "notes", "home"]

    def __init__(self, *args, **kwargs):
       user = kwargs.pop('home')
       super(ApplianceForm, self).__init__(*args, **kwargs)
       self.fields['home'].queryset = Home.objects.filter(owner=user)


class RoofForm(forms.ModelForm):
    class Meta:
       model = Roof
       fields = ["material", "age_of_roof", "notes", "home"]

    def __init__(self, *args, **kwargs):
       user = kwargs.pop('home')
       super(RoofForm, self).__init__(*args, **kwargs)
       self.fields['home'].queryset = Home.objects.filter(owner=user)
