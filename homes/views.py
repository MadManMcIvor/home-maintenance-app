from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from homes.models import Home, Roof, Appliance
from django.contrib.auth.models import User
from homes.forms import ApplianceForm, RoofForm


class HomeDetailView(LoginRequiredMixin, DetailView):
    model = Home
    template_name = "home_details.html"

    def get_queryset(self):
        return Home.objects.filter(owner=self.request.user)

class HomeListView(LoginRequiredMixin, ListView):
    model = Home
    template_name = "home_list.html"

    def get_queryset(self):
        return Home.objects.filter(owner=self.request.user)

class HomeCreateView(LoginRequiredMixin, CreateView):
    model = Home
    template_name = "home_create.html"
    fields = ["address", "nickname", "bedrooms", "bathrooms", "size", "lot_size", "year_built", "purchase_date"]
      
    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("home_list")

class HomeUpdateView(UpdateView):
    model = Home
    template_name = "home_update.html"
    fields = ["address", "nickname", "bedrooms", "bathrooms", "size", "lot_size", "year_built", "purchase_date"]
    success_url = reverse_lazy("home_list")

class HomeDeleteView(DeleteView):
    model = Home
    template_name = "home_delete.html"
    success_url = reverse_lazy("maintenance_list")

class RoofCreateView(LoginRequiredMixin, CreateView):
    model = Roof
    template_name = "home_create.html"
    form_class = RoofForm

    def get_form_kwargs(self):
        kwargs = super(RoofCreateView, self).get_form_kwargs()
        kwargs['home'] = self.request.user
        return kwargs
    
    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("home_details", f"{item.home.id}") #had to tweak this line to work using both the restricitng of the fields in the form and also automatically applying a user name

class RoofUpdateView(UpdateView):
    model = Roof
    template_name = "roof_update.html"
    form_class = RoofForm

    def get_form_kwargs(self):
        kwargs = super(RoofUpdateView, self).get_form_kwargs()
        kwargs['home'] = self.request.user
        return kwargs

    def get_success_url(self):
        return reverse_lazy("home_details", args=[self.object.home.id])

class RoofDeleteView(DeleteView):
    model = Roof
    template_name = "roof_delete.html"

    def get_success_url(self):
        return reverse_lazy("home_details", args=[self.object.home.id])

class ApplianceCreateView(LoginRequiredMixin, CreateView):
    template_name = "appliance_create.html"
    form_class = ApplianceForm

    def get_form_kwargs(self):
        kwargs = super(ApplianceCreateView, self).get_form_kwargs()
        kwargs['home'] = self.request.user
        return kwargs

    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("home_details", f"{item.home.id}")

class ApplianceUpdateView(UpdateView):
    model = Appliance
    template_name = "appliance_update.html"
    form_class = ApplianceForm

    def get_form_kwargs(self):
        kwargs = super(ApplianceUpdateView, self).get_form_kwargs()
        kwargs['home'] = self.request.user
        return kwargs

    def get_success_url(self):
        return reverse_lazy("home_details", args=[self.object.home.id])

class ApplianceDeleteView(DeleteView):
    model = Appliance
    template_name = "appliance_delete.html"

    def get_success_url(self):
        return reverse_lazy("home_details", args=[self.object.home.id])

