from django.contrib import admin
from homes.models import Home, Roof, Appliance

admin.site.register(Home)
admin.site.register(Roof)
admin.site.register(Appliance)