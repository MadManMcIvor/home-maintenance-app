from django.urls import path

from homes.views import (
    HomeDetailView,
    HomeListView,
    HomeCreateView,
    HomeDeleteView,
    HomeUpdateView,
    RoofCreateView,
    RoofUpdateView,
    RoofDeleteView,
    ApplianceCreateView,
    ApplianceUpdateView,
    ApplianceDeleteView,
)

urlpatterns = [
    path("", HomeListView.as_view(), name="home_list"),
    path("<int:pk>/", HomeDetailView.as_view(), name="home_details"),
    path("create/", HomeCreateView.as_view(), name="home_create"),
    path("<int:pk>/update/", HomeUpdateView.as_view(), name="home_update"),
    path("<int:pk>/delete/", HomeDeleteView.as_view(), name="home_delete"),
    path("roof/create", RoofCreateView.as_view(), name="roof_create"),
    path("roof/<int:pk>/update/", RoofUpdateView.as_view(), name="roof_update"),
    path("roof/<int:pk>/delete/", RoofDeleteView.as_view(), name="roof_delete"),
    path("appliance/create", ApplianceCreateView.as_view(), name="appliance_create"),
    path("appliance/<int:pk>/update/", ApplianceUpdateView.as_view(), name="appliance_update"),
    path("appliance/<int:pk>/delete/", ApplianceDeleteView.as_view(), name="appliance_delete"),
]
