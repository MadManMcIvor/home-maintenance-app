from django.db import models
from django.contrib.auth.models import User


class Home(models.Model):
    address = models.CharField(max_length=255)
    nickname = models.CharField(max_length=255, null=True, blank=True)
    BED_CHOICES = (
    ("0", "0"),
    ("1", "1"),
    ("2", "2"),
    ("3", "3"),
    ("4", "4"),
    ("5", "5"),
    ("6", "6"),
    ("7", "7"),
    ("8", "8"),
    ("9", "9"),
    ("10", "10"),
    ("10.5", "I got too many damn bedrooms!"),  
    )
    bedrooms = models.CharField(max_length=10, null=True, blank=True, choices=BED_CHOICES)
    BATH_CHOICES = (
    ("0", "0"),
    ("1", "1"),
    ("1.5", "1.5"),
    ("2", "2"),
    ("2.5", "2.5"),
    ("3", "3"),
    ("3.5", "3.5"),
    ("4", "4"),
    ("4.5", "4.5"),
    ("5", "5"),
    ("5.5", "5.5"),
    ("6", "6"),
    ("6.5", "6.5"),
    ("7", "7"),
    ("7.5", "7.5"),
    ("8", "8"),
    ("8.5", "8.5"),
    ("9", "9"),
    ("9.5", "9.5"),
    ("10", "10"),
    ("10.5", "I got too many damn bathrooms!"), 
    )
    bathrooms = models.CharField(max_length=10, null=True, blank=True, choices=BATH_CHOICES)
    size = models.IntegerField(null=True, blank=True)
    lot_size = models.DecimalField(null=True, blank=True, max_digits=10, decimal_places=3)
    year_built = models.IntegerField(null=True, blank=True)
    purchase_date = models.DateField(null=True, blank=True)
    owner = models.ForeignKey(User, related_name="homes", on_delete=models.CASCADE)

    def __str__(self):
        return self.address
    

class Roof(models.Model):
    ROOF_CHOICES = (
    ("Asphalt", "Asphalt"),
    ("Tile", "Tile"),
    ("Metal", "Metal"),
    ("Slate", "Slate"),
    ("Cedar_Shake", "Cedar Shake"),
    ("Corrugated", "Corrugated"),
    ("Composite", "Composite"),
    ("Flat", "Flat"),
    ("Solar_Tile", "Solar Tile"),
    ("Other", "Other"),
    )
    material = models.CharField(max_length=30, choices=ROOF_CHOICES)
    age_of_roof = models.IntegerField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    home = models.ForeignKey(Home, related_name="roof", on_delete=models.CASCADE)
    owner = models.ForeignKey(User, related_name="roof", on_delete=models.CASCADE)

    def __str__(self):
        return self.material + " Roof"

class Appliance(models.Model):
    APPLIANCE_TYPE = (
    ("Fridge", "Fridge"),
    ("Freezer", "Freezer"),
    ("Ice_Maker", "Ice Maker"),
    ("Washer", "Washer"),
    ("Dryer", "Dryer"),
    ("Dishwasher", "Dishwasher"),
    ("Other", "Other"),
    )
    type = models.CharField(max_length=30, choices=APPLIANCE_TYPE)
    nickname = models.CharField(max_length=255, null=True, blank=True)
    brand = models.CharField(max_length=100, null=True, blank=True)
    model = models.CharField(max_length=150, null=True, blank=True)
    age_of_unit = models.IntegerField(null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    home = models.ForeignKey(Home, related_name="appliances", on_delete=models.CASCADE)
    owner = models.ForeignKey(User, related_name="appliances", on_delete=models.CASCADE)

    def __str__(self):
        return self.nickname








