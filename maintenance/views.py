from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy  # reverse
from maintenance.models import Maintenance
from maintenance.forms import MaintenanceForm


class MaintenanceListView(LoginRequiredMixin, ListView):
    model = Maintenance
    template_name = "maintenance_list.html"

    def get_queryset(self):
        return Maintenance.objects.filter(owner=self.request.user)

class MaintenanceDetailView(LoginRequiredMixin, DetailView):
    model = Maintenance
    template_name = "maintenance_details.html"

    def get_queryset(self):
        return Maintenance.objects.filter(owner=self.request.user)

class MaintenanceCreateView(LoginRequiredMixin, CreateView):
    model = Maintenance
    template_name = "maintenance_create.html"
    form_class = MaintenanceForm
    
    def get_form_kwargs(self):
        kwargs = super(MaintenanceCreateView, self).get_form_kwargs()
        kwargs['home'] = self.request.user
        return kwargs
    
    def form_valid(self, form):
        item = form.save(commit=False)
        item.owner = self.request.user
        item.save()
        return redirect("maintenance_list")


class MaintenanceUpdateView(UpdateView):
    model = Maintenance
    template_name = "maintenance_update.html"
    success_url = reverse_lazy("maintenance_list")
    form_class = MaintenanceForm

    def get_form_kwargs(self):
        kwargs = super(MaintenanceUpdateView, self).get_form_kwargs()
        kwargs['home'] = self.request.user
        return kwargs

class MaintenanceDeleteView(DeleteView):
    model = Maintenance
    template_name = "maintenance_delete.html"
    success_url = reverse_lazy("maintenance_list")