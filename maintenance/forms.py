from django import forms
from maintenance.models import Maintenance
from homes.models import Appliance, Home, Roof
from service_providers.models import ServiceProvider

class MaintenanceForm(forms.ModelForm):
    class Meta:
       model = Maintenance
       fields = ["category", "job", "description", "cost", "servicer", "home", "appliance"]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('home')
        super(MaintenanceForm, self).__init__(*args, **kwargs)
        #These self fields are how I filter the drop down menus in the create/update views
        self.fields['home'].queryset = Home.objects.filter(owner=user)
        self.fields['appliance'].queryset = Appliance.objects.filter(owner=user)
        self.fields['servicer'].queryset = ServiceProvider.objects.filter(added_by=user)



