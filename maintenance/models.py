from unicodedata import category
from django.db import models
from service_providers.models import ServiceProvider
from homes.models import Home, Appliance
from django.contrib.auth.models import User


class Maintenance(models.Model):
    SERVICE_LIST = (
    ("Roof", "Roof"),
    ("HVAC", "HVAC"),
    ("Sewer", "Sewer"),
    ("Electrical", "Electrical"),
    ("Appliance", "Appliance"),
    ("Painting", "Painting"),
    ("Landscaping", "Landscaping"),
    ("General Repair", "General Repair"),
    ("Remodel", "Remodel"),
    ("Other", "Other"),
    )
    category = models.CharField(max_length=40, choices=SERVICE_LIST)
    job = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    cost = models.DecimalField(max_digits=13, decimal_places=2, null=True, blank=True)
    servicer = models.ForeignKey(ServiceProvider, related_name="maintenance", on_delete=models.SET_NULL, null=True, blank=True)
    home = models.ForeignKey(Home, related_name="maintenance", on_delete=models.SET_NULL, null=True, blank=True)
    owner = models.ForeignKey(
        User, null=True, related_name="maintenance", on_delete=models.SET_NULL
    )
    appliance = models.ForeignKey(Appliance, null=True, blank=True, related_name="maintenance", on_delete=models.SET_NULL)


    def __str__(self):
        return "[" + self.category + "] " + self.job