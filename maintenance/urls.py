from django.urls import path

from maintenance.views import (
    MaintenanceListView,
    MaintenanceCreateView,
    MaintenanceDeleteView,
    MaintenanceDetailView,
    MaintenanceUpdateView,
)

urlpatterns = [
    path("", MaintenanceListView.as_view(), name="maintenance_list"),
    path("<int:pk>/", MaintenanceDetailView.as_view(), name="maintenance_details"),
    path("create/", MaintenanceCreateView.as_view(), name="maintenance_create"),
    path("<int:pk>/update/", MaintenanceUpdateView.as_view(), name="maintenance_update"),
    path("<int:pk>/delete/", MaintenanceDeleteView.as_view(), name="maintenance_delete"),
]
