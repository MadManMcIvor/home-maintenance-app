from django.db import models
from django.contrib.auth.models import User

class ServiceProvider(models.Model):
    name = models.CharField(max_length=255)
    company = models.CharField(max_length=255, null=True, blank=True)
    SERVICE_PROVIDER_TYPE = (
    ("Electrician", "Electrician"),
    ("Plumber", "Plumber"),
    ("Landscaper", "Landscaper"),
    ("Handyman", "Handyman"),
    ("Appliance Repair", "Appliance Repair"),
    ("General Contractor", "General Contractor"),
    ("Pest", "Pest"),
    ("Painter", "Painter"),
    ("HVAC Tech", "HVAC Tech"),
    ("Other", "Other"), 
    )
    type = models.CharField(max_length=40, null=True, blank=True, choices=SERVICE_PROVIDER_TYPE)
    phone = models.IntegerField(null=True, blank=True)
    email = models.CharField(max_length=255, null=True, blank=True)
    website = models.CharField(max_length=255, null=True, blank=True)
    notes = models.TextField(null=True, blank=True)
    added_by = models.ForeignKey(User, related_name="servicer_providers", on_delete=models.CASCADE)

    def __str__(self):
        return self.name
