from django.contrib import admin
from service_providers.models import ServiceProvider

admin.site.register(ServiceProvider)