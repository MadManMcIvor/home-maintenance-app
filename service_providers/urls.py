from django.urls import path

from service_providers.views import (
    ServiceProviderListView,
    ServiceProviderDetailView,
    ServiceProviderCreateView,
    ServiceProviderUpdateView,
    ServiceProvdiersDeleteView
)

urlpatterns = [
    path("", ServiceProviderListView.as_view(), name="service_providers_list"),
    path("<int:pk>/", ServiceProviderDetailView.as_view(), name="service_providers_details"),
    path("create/", ServiceProviderCreateView.as_view(), name="service_providers_create"),
    path("<int:pk>/update/", ServiceProviderUpdateView.as_view(), name="service_providers_update"),
    path("<int:pk>/delete/", ServiceProvdiersDeleteView.as_view(), name="service_providers_delete"),
]
