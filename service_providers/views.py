from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView

from django.urls import reverse_lazy  # reverse
from service_providers.models import ServiceProvider

class ServiceProviderListView(LoginRequiredMixin, ListView):
    model = ServiceProvider
    template_name = "service_provider_list.html"

    def get_queryset(self):
        return ServiceProvider.objects.filter(added_by=self.request.user)

class ServiceProviderDetailView(LoginRequiredMixin, DetailView):
    model = ServiceProvider
    template_name = "service_provider_details.html"

    def get_queryset(self):
        return ServiceProvider.objects.filter(added_by=self.request.user)

class ServiceProviderCreateView(LoginRequiredMixin, CreateView):
    model = ServiceProvider
    template_name = "service_provider_create.html"
    fields = ["name", "company", "type", "phone", "email", "website", "notes"]
      
    def form_valid(self, form):
        item = form.save(commit=False)
        item.added_by = self.request.user
        item.save()
        return redirect("service_providers_list")


class ServiceProviderUpdateView(UpdateView):
    model = ServiceProvider
    template_name = "service_providers_update.html"
    fields = ["name", "company", "type", "phone", "email", "website", "notes"]
    success_url = reverse_lazy("service_providers_list")

class ServiceProvdiersDeleteView(DeleteView):
    model = ServiceProvider
    template_name = "service_provider_delete.html"
    success_url = reverse_lazy("service_providers_list")